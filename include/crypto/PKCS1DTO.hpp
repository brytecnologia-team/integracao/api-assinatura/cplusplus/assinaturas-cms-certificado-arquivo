#ifndef __PKCS1DTO_hpp
#define __PKCS1DTO_hpp

#include <string>

class PKCS1DTO
{
public:
	PKCS1DTO();
	PKCS1DTO(long nonce, const std::string& content);
	~PKCS1DTO();

	long get_nonce();
	void set_nonce(long nonce);

	std::string get_content();
	void set_content(const std::string& content);

	std::string get_signature_value();
	void set_signature_value(const std::string& signature_value);

	const char *get_file();
	void set_file(const char *file);

private:
	long _nonce;
	std::string _content;
	std::string _signature_value;
	const char *_file;
};
#endif
