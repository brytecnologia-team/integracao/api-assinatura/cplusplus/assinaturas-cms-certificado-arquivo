#ifndef __signature_config_hpp
#define __signature_config_hpp

#include <string>

#define HASH_ALGORITHM "SHA256"

#define ATTACHED "true"

#define DETACHED "false"

#define PROFILE "BASIC"

#define OPERATION_TYPE "SIGNATURE"

#define NUMBER_OF_DOCUMENTS 1

const std::string ORIGINAL_DOCUMENTS_LOCATION[] = {"./resources/documents/myTextDocument.txt"};

#define OUTPUT_RESOURCE_FOLDER "./resources/generatedSignatures"

#endif
