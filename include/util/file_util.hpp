#ifndef __file_util_hpp
#define __file_util_hpp

#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdio.h>

void write_content_to_file(const char *file_path, const char *file_name, const char *content);

#endif
