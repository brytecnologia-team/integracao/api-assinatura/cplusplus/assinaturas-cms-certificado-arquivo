#ifndef __util_hpp
#define __util_hpp

#include <iostream>
#include <string.h>
#include <algorithm>

#define BEGIN_CERTIFICATE "-----BEGIN CERTIFICATE-----"
#define END_CERTIFICATE "-----END CERTIFICATE-----"

class Util {

private:
	static const std::string base64_chars;

public:
	static std::string remove_substring(const std::string& text, const std::string& to_remove);
	static size_t decode_base64(const std::string& b64_encoded, const char ** decoded_content);
	static std::string encode_base64(const char * data, size_t data_len);
	static std::string cert_pem_to_base64(const std::string& cert_pem);

private:
	static inline bool is_base64(unsigned char c) { return (isalnum(c) || (c == '+') || (c == '/')); }
};

#endif