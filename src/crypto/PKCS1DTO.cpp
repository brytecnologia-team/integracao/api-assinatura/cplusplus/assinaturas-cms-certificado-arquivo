#include "crypto/PKCS1DTO.hpp"

PKCS1DTO::PKCS1DTO()
: _nonce(0) {};

PKCS1DTO::PKCS1DTO(long nonce, const std::string& content)
: _nonce(nonce), _content(content) {};

PKCS1DTO::~PKCS1DTO()
{
}

long PKCS1DTO::get_nonce() { return _nonce; };
void PKCS1DTO::set_nonce(long nonce) { _nonce = nonce; };

std::string PKCS1DTO::get_content() { return _content; };
void PKCS1DTO::set_content(const std::string&  content) { _content = content; };

std::string PKCS1DTO::get_signature_value() { return _signature_value; };
void PKCS1DTO::set_signature_value(const std::string& signature_value) { _signature_value = signature_value; };

const char *PKCS1DTO::get_file() { return _file; }
void PKCS1DTO::set_file(const char *file) { _file = file; };
