#include "crypto/signer.hpp"

Signer::Signer(const char *keystore_path, const char *keystore_password)
{
	FILE *fp;
	X509 *cert;
	PKCS12 *keystore;

	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();
	if(!(fp = fopen(keystore_path, "rb")))
	{
		std::cout << "Keystore nao encontrado" << std::endl;
		exit(1);
	}

	keystore = d2i_PKCS12_fp(fp, NULL);
	fclose(fp);
	if(!(PKCS12_parse(keystore, keystore_password, &priv_key, &cert, NULL)))
	{
		std::cout << "Senha do keystore incorreta" << std::endl;
		exit(1);
	}

	const char *data;
	BIO *bio = BIO_new(BIO_s_mem());
	PEM_write_bio_X509(bio, cert);
	size_t len = BIO_get_mem_data(bio, &data);
	cert_pem = std::string(data, len);

	BIO_free(bio);
	X509_free(cert);
	PKCS12_free(keystore);
}

Signer::~Signer()
{
	EVP_PKEY_free(priv_key);
}

std::string& Signer::get_certificate() { return cert_pem; };

void Signer::sign(const char *hash_algorithm, const char *data, size_t data_len, unsigned char **sigret, size_t **s_len_ret)
{
	const EVP_MD *md = EVP_get_digestbyname(hash_algorithm);
	EVP_MD_CTX *mdctx = EVP_MD_CTX_create();
	unsigned char *sig;
	size_t s_len = 0;

	if(1 != EVP_DigestSignInit(mdctx, NULL, md, NULL, priv_key))
	{
		std::cout << "Failed to init signature" << std::endl;
		exit(1);
	}
	if(1 != EVP_DigestSignUpdate(mdctx, data, data_len))
	{
		std::cout << "Failed to add signature data" << std::endl;
		exit(1);
	}

	if(1 != EVP_DigestSignFinal(mdctx, NULL, &s_len))
	{
		std::cout << "Failed to calculate signature size" << std::endl;
		exit(1);
	}

	sig = new unsigned char[s_len];

	if(1 != EVP_DigestSignFinal(mdctx, sig, &s_len))
	{
		std::cout << "Failed to generate signature" << std::endl;
		exit(1);
	}
	*sigret = sig;
	*s_len_ret = &s_len;

	EVP_MD_CTX_free(mdctx);

}
