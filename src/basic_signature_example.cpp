#include "config/certificate_config.hpp"
#include "config/service_config.hpp"
#include "config/signature_config.hpp"
#include "crypto/PKCS1DTO.hpp"
#include "crypto/signer.hpp"
#include <stdio.h>
#include <iostream>
#include <string>
#include <curl/curl.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <json-c/json.h>
#include "util/file_util.hpp"
#include "util/http_util.hpp"
#include "util/util.hpp"
#include <sstream>

PKCS1DTO *initialize_signature(Signer *signer);
void sign_content(Signer *signer, PKCS1DTO *pKCS1DTO_list);
char **finalize_signature(Signer *signer, PKCS1DTO *pKCS1DTO_list);

int main(int argc, char **argv)
{
	// Step 1 - Load PrivateKey and Certificate
	Signer *signer = new Signer(PRIVATE_KEY_LOCATION, PRIVATE_KEY_PASSWORD);

	// Step 2 - Signature initialization.
	PKCS1DTO *pKCS1DTO_list = initialize_signature(signer);

	// Step 3 - Local encryption of signed attributes using private key.
	sign_content(signer, pKCS1DTO_list);

	// Step 4 - Signature finalization.
	char **signatures = finalize_signature(signer, pKCS1DTO_list);

	// Step 5 - Write signatures to files.
	for (int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		std::string file_name("signature-item-" + std::to_string(i + 1));
		write_content_to_file(OUTPUT_RESOURCE_FOLDER, file_name.c_str(), signatures[i]);
	}

	for (int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
		delete[] signatures[i];
	delete[] signatures;
	delete[] pKCS1DTO_list;
	delete signer;
}

PKCS1DTO *initialize_signature(Signer *signer) {
	CURL *curl;

	curl_global_init(CURL_GLOBAL_ALL);

	curl = curl_easy_init();
	curl_mime *mime;
	curl_mimepart *part;

	mime = curl_mime_init(curl);

	part = curl_mime_addpart(mime);
	curl_mime_data(part, std::to_string(rand()).c_str(), CURL_ZERO_TERMINATED);
	curl_mime_name(part, "nonce");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, ATTACHED, CURL_ZERO_TERMINATED);
	curl_mime_name(part, "attached");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, PROFILE, CURL_ZERO_TERMINATED);
	curl_mime_name(part, "profile");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, HASH_ALGORITHM, CURL_ZERO_TERMINATED);
	curl_mime_name(part, "hashAlgorithm");

	std::string cert_b64 = Util::cert_pem_to_base64(signer->get_certificate());

	part = curl_mime_addpart(mime);
	curl_mime_data(part, cert_b64.c_str(), CURL_ZERO_TERMINATED);
	curl_mime_name(part, "certificate");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, "SIGNATURE", CURL_ZERO_TERMINATED);
	curl_mime_name(part, "operationType");

	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		part = curl_mime_addpart(mime);
		curl_mime_data(part, std::to_string(rand()).c_str(), CURL_ZERO_TERMINATED);
		curl_mime_name(part, std::string("originalDocuments[" + std::to_string(i) + "][nonce]").c_str());

		std::ifstream file(ORIGINAL_DOCUMENTS_LOCATION[i].c_str());
		if (!file.good())
		{
			std::cout << "Failed to read file: " << ORIGINAL_DOCUMENTS_LOCATION[i].c_str() << std::endl;
			exit(1);
		}
		file.close();

		part = curl_mime_addpart(mime);
		curl_mime_filedata(part, ORIGINAL_DOCUMENTS_LOCATION[i].c_str());
		curl_mime_name(part, std::string("originalDocuments[" + std::to_string(i) + "][content]").c_str());
	}

	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "Content-Type: multipart/form-data");
	headers = curl_slist_append(headers, "Authorization: Bearer " ACCESS_TOKEN);

	std::string *response_body = new std::string();
	curl_easy_setopt(curl, CURLOPT_URL, URL_INITIALIZE_SIGNATURE);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_MIMEPOST, mime);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_response_body);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &response_body);

	curl_easy_perform(curl);

	long status_code;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status_code);

	if(status_code != 200)
	{
		std::cout << "Error during signature initialization - Status code: " << status_code << std::endl;
		std::cout << response_body->c_str() << std::endl;
		delete response_body;
		exit(1);
	}

	std::cout << "Signature initialization JSON response: " << response_body->c_str() << std::endl;

	json_object *response_body_as_json = json_tokener_parse(response_body->c_str());

	json_object *signed_attributes_json_array;
	json_object_object_get_ex(response_body_as_json, "signedAttributes", &signed_attributes_json_array);

	PKCS1DTO *pKCS1DTO_list = new PKCS1DTO[NUMBER_OF_DOCUMENTS];

	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		json_object *signed_attributes_as_json = json_object_array_get_idx(signed_attributes_json_array, i);

		json_object *content_as_json;
		json_object_object_get_ex(signed_attributes_as_json, "content", &content_as_json);

		json_object *nonce_as_json;
		json_object_object_get_ex(signed_attributes_as_json, "nonce", &nonce_as_json);

		long nonce = json_object_get_int64(nonce_as_json);
		const char *content_as_string = json_object_get_string(content_as_json);

		std::string content(content_as_string);


		pKCS1DTO_list[i].set_nonce(nonce);
		pKCS1DTO_list[i].set_content(content);
		pKCS1DTO_list[i].set_file(ORIGINAL_DOCUMENTS_LOCATION[i].c_str());
	}

	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_mime_free(mime);
	delete response_body;
	json_object_put(response_body_as_json);

	return pKCS1DTO_list;

}

void sign_content(Signer *signer, PKCS1DTO *pKCS1DTO_list)
{
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		const char *decoded_content;
		size_t decoded_content_len = Util::decode_base64(pKCS1DTO_list[i].get_content(), &decoded_content);

		unsigned char *sig;
		size_t *s_len;

		signer->sign(HASH_ALGORITHM, decoded_content, decoded_content_len, &sig, &s_len);

		std::string b64_encoded_signature = Util::encode_base64((const char *) sig, *s_len);

		pKCS1DTO_list[i].set_signature_value(b64_encoded_signature);

		delete[] decoded_content;
		delete[] sig;
	}
}

char **finalize_signature(Signer *signer, PKCS1DTO *pKCS1DTO_list)
{
	CURL *curl;

	curl = curl_easy_init();
	curl_mime *mime;
	curl_mimepart *part;

	mime = curl_mime_init(curl);

	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		part = curl_mime_addpart(mime);

		std::ostringstream oss;
		oss << pKCS1DTO_list[i].get_nonce();
		curl_mime_data(part, oss.str().c_str(), CURL_ZERO_TERMINATED);

		curl_mime_name(part, std::string("finalizations[" + std::to_string(i) + "][nonce]").c_str());

		part = curl_mime_addpart(mime);
		curl_mime_data(part, pKCS1DTO_list[i].get_content().c_str(), CURL_ZERO_TERMINATED);
		curl_mime_name(part, std::string("finalizations[" + std::to_string(i) + "][signedAttributes]").c_str());

		part = curl_mime_addpart(mime);
		curl_mime_data(part, (char *) pKCS1DTO_list[i].get_signature_value().c_str(), CURL_ZERO_TERMINATED);
		curl_mime_name(part, std::string("finalizations[" + std::to_string(i) + "][signatureValue]").c_str());

		part = curl_mime_addpart(mime);
		curl_mime_filedata(part, pKCS1DTO_list[i].get_file());
		curl_mime_name(part, std::string("finalizations[" + std::to_string(i) + "][document]").c_str());
	}

	part = curl_mime_addpart(mime);
	curl_mime_data(part, std::to_string(rand()).c_str(), CURL_ZERO_TERMINATED);
	curl_mime_name(part, "nonce");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, ATTACHED, CURL_ZERO_TERMINATED);
	curl_mime_name(part, "attached");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, PROFILE, CURL_ZERO_TERMINATED);
	curl_mime_name(part, "profile");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, HASH_ALGORITHM, CURL_ZERO_TERMINATED);
	curl_mime_name(part, "hashAlgorithm");

	std::string cert_b64 = Util::cert_pem_to_base64(signer->get_certificate());

	part = curl_mime_addpart(mime);
	curl_mime_data(part, cert_b64.c_str(), CURL_ZERO_TERMINATED);
	curl_mime_name(part, "certificate");

	part = curl_mime_addpart(mime);
	curl_mime_data(part, "SIGNATURE", CURL_ZERO_TERMINATED);
	curl_mime_name(part, "operationType");

	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "Content-Type: multipart/form-data");
	headers = curl_slist_append(headers, "Authorization: Bearer " ACCESS_TOKEN);

	std::string *response_body = new std::string();
	curl_easy_setopt(curl, CURLOPT_URL, URL_FINALIZE_SIGNATURE);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_MIMEPOST, mime);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_response_body);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &response_body);

	curl_easy_perform(curl);

	long status_code;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status_code);

	if(status_code != 200)
	{
		std::cout << "Error during signature finalization - Status code: " << status_code << std::endl;
		std::cout << response_body->c_str() << std::endl;
		exit(1);
	}

	std::cout << "Signature finalization JSON response: " << response_body->c_str() << std::endl;

	json_object *response_body_as_json = json_tokener_parse(response_body->c_str());

	json_object *signatures_array_json;
	json_object_object_get_ex(response_body_as_json, "signatures", &signatures_array_json);

	char **signatures = new char*[NUMBER_OF_DOCUMENTS];
	for(int i = 0; i < NUMBER_OF_DOCUMENTS; i++)
	{
		json_object *signature_as_json = json_object_array_get_idx(signatures_array_json, i);

		json_object *content_as_json;
		json_object_object_get_ex(signature_as_json, "content", &content_as_json);

		const char *content_as_string = json_object_get_string(content_as_json);

		signatures[i] = new char[strlen(content_as_string) + 1];
		strcpy(signatures[i], content_as_string);

		std::cout << "Successful signature generation: " << signatures[i] << std::endl;
	}

	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_mime_free(mime);
	delete response_body;
	json_object_put(response_body_as_json);

	return signatures;
}
