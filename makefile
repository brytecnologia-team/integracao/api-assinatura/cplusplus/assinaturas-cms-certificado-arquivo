include ./makedefs

CRYPTO_SRC_FILES = $(wildcard $(CRYPTO)/*.cpp)
CRYPTO_OBJ_FILES = $(patsubst $(CRYPTO)/%.cpp, $(CRYPTO)/%.o, $(CRYPTO_SRC_FILES))
UTIL_SRC_FILES   = $(wildcard $(UTIL)/*.cpp)
UTIL_OBJ_FILES   = $(patsubst $(UTIL)/%.cpp, $(UTIL)/%.o, $(UTIL_SRC_FILES))
OBJ_FILES        = $(CRYPTO_OBJ_FILES) $(UTIL_OBJ_FILES)
	
all: attached detached

attached: $(OBJ_FILES) $(SRC)/basic_signature_example.o
	$(CXX) -o $@ $(OBJ_FILES) $(SRC)/basic_signature_example.o $(CXXFLAGS)

detached: $(OBJ_FILES) $(SRC)/basic_signature_detached_example.o
	$(CXX) -o $@ $(OBJ_FILES) $(SRC)/basic_signature_detached_example.o $(CXXFLAGS)
	
%.o: %.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

$(OBJ_FILES):
	$(MAKE) -C $(CRYPTO)
	$(MAKE) -C $(UTIL)

.PHONY: clean

clean:
	$(MAKE) -C $(CRYPTO) clean
	$(MAKE) -C $(UTIL) clean
	rm -f $(SRC)/*.o attached detached core resources/generatedSignatures/* $(SRC)/*~ $(INCLUDE)/*~
	